FROM python:3.10

RUN groupadd flask && \
    useradd -r -g flask flask

RUN apt update && apt upgrade -y

WORKDIR /code

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

RUN chown -R flask:flask ./

USER flask

EXPOSE 5000

CMD ["python", "main.py"]

